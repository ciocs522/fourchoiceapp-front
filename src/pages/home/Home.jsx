import React from 'react'
import './Home.css'
import Content from '../../components/home/content/Content'

export default function home() {
  return (
    <>
      <div className="home">
        <Content className="content" link={'fe'}/>
      </div>
    </>
  )
}
