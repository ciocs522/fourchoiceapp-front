import React from 'react'
import Answer from '../answer/Answer'
import dummyProblem from '../../problems'

export default function Fe() {
  return (
    <Answer content = {dummyProblem} />
  )
}
