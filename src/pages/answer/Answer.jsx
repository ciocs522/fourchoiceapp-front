import React from 'react'
import { useState } from 'react'
import './Answer.css'

export default function Answer({ content }) {
  const problems = content.problems;

  const numOfProblems = problems.length;
  const [currentIndex, setCurrentIndex] = useState(-1);
  const [title, setTitle] = useState("press button to start");
  const [correctAnsIndex, setCorrectAnsIndex] = useState(0);
  const [choice1, setChoice1] = useState("start");
  const [choice2, setChoice2] = useState("start");
  const [choice3, setChoice3] = useState("start");
  const [choice4, setChoice4] = useState("start");
  const [isResMode, setIsResMode] = useState(false);
  const [isCorrected, setIsCorrected] = useState(false);

  const update = (() => {
    setTitle(problems[currentIndex + 1].state);
    const choices = [problems[currentIndex + 1].correctAnswer,
    ...problems[currentIndex + 1].wrongAnswers];
    const shuffledChoices = shuffle(choices);
    for (let i = 0; i < 4; i++) {
      if (shuffledChoices[i] === problems[currentIndex + 1].correctAnswer) {
        setCorrectAnsIndex(i);
      }
      let txt = shuffledChoices[i]
      switch (i) {
        case 0:
          setChoice1(txt);
          break;
        case 1:
          setChoice2(txt);
          break;
        case 2:
          setChoice3(txt);
          break;
        case 3:
          setChoice4(txt);
          break;
        default:
          return;
      }
    }
  })

  const judge = (index => {
    if (index === correctAnsIndex) {
      setIsCorrected(true);
    }
    else {
      setIsCorrected(false);
    }
    setIsResMode(true);
  })

  const toNextProblem = (() => {
    if (currentIndex + 1 === numOfProblems) {
      window.location.replace('../result');
      return;
    }

    setCurrentIndex(currentIndex + 1);
    update();
    setIsResMode(false);
  });

  const shuffle = ([...array]) => {
    for (let i = array.length - 1; i >= 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

  return (
    <>
      <div className='answer'>
        <div className="answerHeader">
          <p className='kari'>{currentIndex + 1}/{numOfProblems}</p>
        </div>

        <div className="screenContainer">
          <div className="screen">
            <span className="questionText">{title}</span>
          </div>
        </div>

        <div className="answerScreen">
          <div className="choices">
            <div className="choice">
              <button className="choiceButton" onClick={() => judge(0)}>{choice1}</button>
            </div>
            <div className="choice">
              <button className="choiceButton" onClick={() => judge(1)}>{choice2}</button>
            </div>
            <div className="choice">
              <button className="choiceButton" onClick={() => judge(2)}>{choice3}</button>
            </div>
            <div className="choice">
              <button className="choiceButton" onClick={() => judge(3)}>{choice4}</button>
            </div>
          </div>
          <div className="next">
            <button className="nextButton" onClick={toNextProblem} style={{ backgroundColor : (isResMode && isCorrected) ? "green" : "blue"}} />
          </div>
        </div>
      </div>
    </>
  )
}
