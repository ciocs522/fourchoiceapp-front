import Home from "./pages/home/Home";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Answer from "./pages/answer/Answer";
import Login from "./pages/login/Login";

import { dummyProblem } from './problems'
import Result from "./pages/result/Result";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/home" element={<Home />} />
        <Route path="/answer" element={<Answer />} />
        <Route path="/result" element={<Result />} />

        <Route path="/answer/fe" element={<Answer content = {dummyProblem} />} />

      </Routes>
    </Router>
  );
}

export default App;
