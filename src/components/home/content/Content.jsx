import React from 'react'
import { Link } from 'react-router-dom'
import './Content.css'

export default function Content({ link }) {
  return (
    <>
      <div className="content">
        <Link to={'/answer/' + link}>基本情報</Link>
      </div>
    </>
  )
}
