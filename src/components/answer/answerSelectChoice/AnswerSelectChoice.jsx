import React from 'react'
import './AnswerSelectChoice.css'

export default function AnswerSelectChoice({ callback }) {
  const toNextQuestion = (() => {
      //次の問題の選択肢を表示
  })

  const onClickButton = (() => {
    toNextQuestion();
    callback();
  })

  return (
    <div className="answerScreen">
      <div className="displayArea">
        <div className="choice">
          <button className="choiceButton">button</button>
        </div>
        <div className="choice">
          <button className="choiceButton">button</button>
        </div>
        <div className="choice">
          <button className="choiceButton">button</button>
        </div>
        <div className="choice">
          <button className="choiceButton">button</button>
        </div>
      </div>
    </div>
  )
}
